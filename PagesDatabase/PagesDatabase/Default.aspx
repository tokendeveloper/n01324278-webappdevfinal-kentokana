﻿<%@ Page Title="Home Page" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="PagesDatabase._Default" %>

<%-- For whatever reason, the user control defined in the web.config file isn't being read property so I adde this manually.--%>
<%-- https://forums.asp.net/t/1149596.aspx?Element+a+user+control+is+not+a+known+element+ --%>

<asp:Content ID="pageTitle" ContentPlaceHolderID="ContentTitle" runat="server">
    <h1>View Pages</h1>
</asp:Content>


<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <div runat="server" id="AddPageStatusDiv">

    </div>
    <div runat="server" id="DeletePageStatus">

    </div>
    <asp:SqlDataSource
        runat="server"
        ID="pagesDB"
        ConnectionString="<%$ ConnectionStrings:pages_db %>"></asp:SqlDataSource>

    <asp:Label runat="server" AssociatedControlID="SearchByTitle">Search by Page Title:</asp:Label>
    <div class="form-group">
        <asp:TextBox class="formEl" runat="server" ID="SearchByTitle" ></asp:TextBox>
        <%-- I was intending on implementing a drop-down list where it gives you options to search by title or by name.
            the query would change based on which drop-down option is selected. 
            
            I also wanted to implement an option for sorting information - either by author or title. 
            It'd be a radio button, or a dropdown list. --%>
    </div>

    <div class="form-group">
        <asp:DropDownList class="formEl" runat="server" ID="pickPublishStatus">
            <asp:ListItem Value="all" runat="server">View All</asp:ListItem>
            <asp:ListItem Value="published" runat="server">View Published Pages</asp:ListItem>
            <asp:ListItem Value="unpublished" runat="server">View Unpublished Pages</asp:ListItem>
        </asp:DropDownList>
    </div>
    <div class="form-group">
        <asp:Button runat="server" class="buttonEl" Text="View" ID="submitViewOption" OnClick="SubmitViewOption" />
    </div>


    <asp:DataGrid runat="server" ID="PagesDataGrid"></asp:DataGrid>


</asp:Content>

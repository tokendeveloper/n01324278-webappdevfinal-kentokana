﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace PagesDatabase
{
    public partial class WebForm2 : System.Web.UI.Page
    {
        private string insert_basequery { get; set; } = "INSERT INTO pages VALUES";

        protected void Page_Load(object sender, EventArgs e)
        {
        }

        protected void Submit(object sender, EventArgs e)
        {

            string pageTitle = titleForm.Text;
            string pageContent = pageContentForm.Text;
            string pageAuthor = authorForm.Text;


            //Fix querystring so that if there is an apostrophe in the form text, it replaces it with ''. 
            //https://stackoverflow.com/questions/7747370/how-to-replace-apostrophe-with-double-apostrophe-in-string
            string pageTitleFormat = pageTitle.Replace("'", "''");
            string pageContentFormat = pageContent.Replace("'", "''");
            string authorFormat = pageAuthor.Replace("'", "''");
            int publishState;

            insert_basequery += "(" +
            "NEXT VALUE FOR PageIdSequence, '"
            + pageTitleFormat + "', '" + pageContentFormat
            + "', '" + authorForm.Text;

            //Publish Date is only added if publish state is 1.
            if (PublishRadioBtn.Checked)
            {
                publishState = 1;
                insert_basequery += "', SYSDATETIME(), " + publishState + ")";


            }
            else if (SaveRadioBtn.Checked)
            {
                publishState = 2;
                insert_basequery += "', NULL, " + publishState + ")";
            }

            try
            {
                insertIntoPagesDB.InsertCommand = insert_basequery;
                insertIntoPagesDB.Insert();
                Server.Transfer("~/Default.aspx?AddPage=Success");
            }

            catch (Exception error)
            {
                InsertStatus.InnerHtml = "That's gonna be a no form me dog. <br>" + error;
            }

            //Use a sequence created by TSQL query. 


            //insertIntoPagesDB.InsertCommand = insert_basequery;
            //insertIntoPagesDB.Insert();

            //Routes to the home page when button is clicked and content is inserted into the DB.
            //Server.Transfer("Default.aspx");


        }


    }
}
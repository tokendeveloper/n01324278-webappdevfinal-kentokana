﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace PagesDatabase
{
    public partial class WebForm1 : System.Web.UI.Page
    {
        private string page_basequery { get; set; } = "SELECT pageContent AS 'Content', pageTitle, authorName, publishDate FROM pages ";

        protected void Page_Load(object sender, EventArgs e)
        {
            string pageId = Request.QueryString["pageId"];
            page_basequery += "WHERE pageId = " + pageId;

            pagesDB.SelectCommand = page_basequery;

            //pageBind() is not necessary for this, as I'm not using a DataGrid here.
            //Just leaving the code here in case I find having the DataGrid useful later.
            //SinglePageDataGrid.DataSource = pageBind(pagesDB);
            SinglePageDataGrid.DataBind();

            DataView viewPage = (DataView)pagesDB.Select(DataSourceSelectArguments.Empty);
            string heading = viewPage[0]["pageTitle"].ToString();
            pageHeading.InnerHtml = heading;

            string author = viewPage[0]["authorName"].ToString();
            AuthorDiv.InnerHtml += author + "<br>";

            string date = viewPage[0]["publishDate"].ToString();
            DateDiv.InnerHtml += date + "</br>";

            string content = viewPage[0]["Content"].ToString();
            PageContentDiv.InnerHtml += content + "</br>";

            string updateStatus = Request.QueryString["Update"];
            

            //if updateStatus querystring in the url returns True, then display update status div.
            if (updateStatus == "True")
            {
                //Bootstrap dismissable button. https://getbootstrap.com/docs/3.3/javascript/#alerts//
                UpdateStatusDiv.InnerHtml = "<p class=\"alert alert-success\">The page was successfully updated! <button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-label=\"Close\"><span aria-hidden = \"true\">&times;</ span ></ button ></p>";
            }



        }

        //protected void EditPage(object sender, EventArgs e)
        //{
        //    Server.Transfer("EditPage.aspx");
        //}

        //DeletePage function that triggers when Delete Page button is clicked.
        //Again, looked at Christine's example for deleting a class. Thanks Christine! :D 
        protected void DeletePage(object sender, EventArgs e)
        {
            string pageId = Request.QueryString["pageId"];
            string deleteStatment = "DELETE pages WHERE pageId = " + pageId;
            //access SqlDataSource deletePage and executes delete statement on deletePage.Delete();
            deletePage.DeleteCommand = deleteStatment;
            deletePage.Delete();

            //https://stackoverflow.com/questions/23976683/asp-net-button-to-redirect-to-another-page
            //Transfers user to the Default.aspx page upon deleting the data row.
            //Add a querystring, so that when the site transfers to Default.aspx, it's able to display the delete status.
            Response.Redirect("Default.aspx?DeleteStat=Success");
        }

        protected void EditThisPage(object sender, EventArgs e)
        {
            Server.Transfer("~/PageEdit.aspx");
        }
    }
}
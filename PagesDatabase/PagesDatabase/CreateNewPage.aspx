﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="CreateNewPage.aspx.cs" Inherits="PagesDatabase.WebForm2" %>

<asp:Content ID="pageTitle" ContentPlaceHolderID="ContentTitle" runat="server">
    <h1>Create A New Page</h1>
</asp:Content>

<asp:Content ID="CreatePageForm" ContentPlaceHolderID="MainContent" runat="server">

    <asp:SqlDataSource
        runat="server"
        ID="insertIntoPagesDB"
        ConnectionString="<%$ ConnectionStrings:pages_db %>"></asp:SqlDataSource>
    <div id="InsertStatus" runat="server">
    </div>

    <div class="form-group">
        <asp:Label runat="server" AssociatedControlID="PublishRadioBtn">Publish Page</asp:Label>
        <asp:RadioButton runat="server" GroupName="PublishState" ID="PublishRadioBtn" />
    </div>
    <div class="form-group">
        <asp:Label runat="server" AssociatedControlID="SaveRadioBtn">Save Without Publishing</asp:Label>
        <asp:RadioButton runat="server" GroupName="PublishState" ID="SaveRadioBtn" />
    </div>

    <div class="form-group">
        <div>
            <asp:Label Text="Enter Page Title:" AssociatedControlID="titleForm" runat="server">Your Page Title:</asp:Label>
        </div>
        <asp:TextBox CssClass="formEl" ID="titleForm" runat="server" name="titleForm"></asp:TextBox>
        <asp:RequiredFieldValidator runat="server" ID="titleFormValidator" ControlToValidate="titleForm" ErrorMessage="Please enter a title for this page."></asp:RequiredFieldValidator>
    </div>
    <div class="form-group">
        <div>
            <asp:Label Text="Enter Author:" AssociatedControlID="authorForm" runat="server">Author:</asp:Label>
        </div>
        <asp:TextBox CssClass="formEl" ID="authorForm" runat="server" />
        <asp:RequiredFieldValidator runat="server" CssClass="error-text" ID="RequiredFieldValidator1" ControlToValidate="authorForm" ErrorMessage="Please enter the author for this page."></asp:RequiredFieldValidator>
    </div>

    <div class="form-group">
        <div>
            <asp:Label Text="Enter Content Here:" AssociatedControlID="pageContentForm" runat="server">Your Page Content:</asp:Label>
        </div>
        <asp:TextBox CssClass="formEl" ID="pageContentForm" TextMode="multiline" Columns="100" Rows="10" runat="server" />
        <%-- Should add logic for checking whether or not the client wants to leave the content blank, if it's left blank. --%>
    </div>
    <%-- https://stackoverflow.com/questions/4508051/textarea-control-asp-net-c-sharp --%>
    <div class="form-group">
        <asp:Button CssClass="buttonEl" runat="server" Text="Submit" ID="SubmitButton" OnClick="Submit" />
    </div>



</asp:Content>

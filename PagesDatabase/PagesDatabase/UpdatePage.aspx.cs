﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace PagesDatabase
{
    public partial class WebForm3 : System.Web.UI.Page
    {
        private string pageQuery { get; set; } = "SELECT pageContent, pageTitle, authorName, publishDate, publishStatus FROM pages WHERE pageId = ";
        private string updateQuery { get; set; } = "UPDATE pages SET ";
        private int publishState { get; set; }


        public int pageId
        {
            get { return Convert.ToInt32(Request.QueryString["pageId"]); }
        }

        protected override void OnPreRender(EventArgs e)
        {


        }


        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                base.OnPreRender(e);
                pageQuery += pageId;


                UpdatePage.SelectCommand = pageQuery;

                System.Data.DataView viewPage = (System.Data.DataView)UpdatePage.Select(DataSourceSelectArguments.Empty);
                string ptitle = viewPage[0]["pageTitle"].ToString();
                string pcontent = viewPage[0]["pageContent"].ToString();
                string pageAuthor = viewPage[0]["authorName"].ToString();
                string publishStat = viewPage[0]["publishStatus"].ToString();

                if (publishStat == "1")
                {
                    PublishRadioBtn.Checked = true;
                }
                else
                {
                    Unpublish.Checked = true;

                }

                titleForm.Text = ptitle;
                pageContentForm.Text = pcontent;
                authorForm.Text = pageAuthor;

                debugDiv.InnerHtml = pageQuery;
                Server.Transfer("UpdatePage.aspx.cs?pageId=" + pageId+"&HelloWorld=true");
            }
        }

        protected void EditThisPage(object sender, EventArgs e)
        {

            string pageTitle = titleForm.Text;
            string pageContent = pageContentForm.Text;
            string pageAuthor = authorForm.Text;

            string pageAuthorFormat = pageAuthor.Replace("'", "''");
            string pageTitleFormat = pageTitle.Replace("'", "''");
            string pageContentFormat = pageContent.Replace("'", "''");


            updateQuery += "authorName ='" + pageAuthorFormat + "', pageTitle = '" + pageTitleFormat + "', pageContent = '" + pageContentFormat;
            System.Data.DataView updatePage = (System.Data.DataView)UpdatePage.Select(DataSourceSelectArguments.Empty);
            //UpdatePage.UpdateCommand = updateQuery;
            //UpdatePage.Update();

            debugDiv2.InnerHtml = updateQuery;

            if (PublishRadioBtn.Checked)
            {
                publishState = 1;
                updateQuery += "', publishDate = SYSDATETIME()" + ", publishStatus = " + publishState + " WHERE pageId = " + pageId;

            }

            else if (Unpublish.Checked)
            {
                //Remove publish date if unpublished
                publishState = 2;
                updateQuery += "', publishDate = NULL" + ", publishStatus =" + publishState + " WHERE pageId = " + pageId;

            }

            try
            {
                UpdatePage.UpdateCommand = updateQuery;
                UpdatePage.Update();
                Response.Redirect("~/SinglePageView.aspx?pageId=" + pageId + "&Update=True");
            }

            catch (Exception error)
            {
                UpdateStatus.InnerHtml = "<h2>The page was not updated.</h2> <br>" + error;
            }

        }

    }
}
﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace PagesDatabase
{
    public partial class _Default : Page
    {
        //Needless(?) to say, lots of code referenced from Christine's students/teachers/class DB example.
        private string pages_basequery { get; set; } = "SELECT pageId, pageTitle AS 'Pages', authorName AS 'Author', publishDate AS 'Date Published' FROM pages ";

        protected void Page_Load(object sender, EventArgs e)
        {
            string publishStat = Request.QueryString["publishStatus"];
            string pickTitle = Request.QueryString["pageTitle"];
            string addPageStatus = Request.QueryString["AddPage"];
            string deletePageStatus = Request.QueryString["DeleteStat"];
            string pickTitleFormat;

            //If deletePageStatus query string in the URL returns "success", create a 'dismissable alert box' when the page is redirected.
            if (deletePageStatus == "Success")
            {
                DeletePageStatus.InnerHtml = "<p class=\"alert alert-success\">The page was successfully deleted. <button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-label=\"Close\"><span aria-hidden = \"true\">&times;</ span ></ button ></p>";
            }

            //If addPageStatus query string in the URL returns "success", create a 'dismissable alert box' when the page is redirected.
            if (addPageStatus == "Success")
            {
                AddPageStatusDiv.InnerHtml = "<p class=\"alert alert-success\">The page was successfully updated! <button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-label=\"Close\"><span aria-hidden = \"true\">&times;</ span ></ button ></p>";
            }

            //Pick Publish Status
            if (publishStat == "1")
            {
                pages_basequery += "WHERE publishStatus = 1 ";
                pickPublishStatus.SelectedValue = "1";
            }
            else if (publishStat == "2")
            {
                pages_basequery += "WHERE publishStatus = 2 ";
                pickPublishStatus.SelectedValue = "2";

            }
            else if (publishStat == null || publishStat == "")
            {
                pages_basequery += "WHERE publishStatus IN (1, 2) ";
                pickPublishStatus.SelectedValue = "0";

            }

            //Title Search Bar
            //If search bar is empty, don't add a LIKE statement for  pageTitle.
            if (pickTitle == "" || pickTitle == null)
            {
                pages_basequery = pages_basequery;
            }
            else
            {
                pickTitleFormat = pickTitle.Replace("+", "%").Replace("'","''");
                pages_basequery += "AND pageTitle LIKE '%" + pickTitleFormat + "%'";
            }

            pagesDB.SelectCommand = pages_basequery;
            PagesDataGrid.DataSource = pageBind(pagesDB);
            PagesDataGrid.DataBind();

        }

        //pageBind function, which takes SqlDataSource as the parameter. 
        protected DataView pageBind(SqlDataSource src)
        {
            DataTable table;
            DataView view;
            table = ((DataView)src.Select(DataSourceSelectArguments.Empty)).Table;


            //Generate dynamic link to individual page view.
            foreach (DataRow row in table.Rows)
            {
                row["Pages"] =
                    "<a class='tableLink' href=\"SinglePageView.aspx?pageId="
                    + row["pageId"]
                    + "\">"
                    + row["Pages"]
                    + "</a>";
            }

            //Remove pageId column.
            table.Columns.Remove("pageId");
            view = table.DefaultView;
            return view;
        }

        //Picking a view of the pages based on filters. 
        //Sends query to url so that we can retrieve this on pageload to get a specific view.
        //I thought of this logic myself - but you might have a logic similar to this. Just wanted to mention it to avoid plagiarism.
        protected void SubmitViewOption(object sender, EventArgs e)
        {

            string publishStatusURL = "";

            //Check the value of the dropdown menu item and adjust the querystring sent to the url.
            string viewChoice = pickPublishStatus.SelectedValue;
            if (viewChoice == "all") 
            {
                publishStatusURL += "publishStatus";
            }

            else if (viewChoice == "published")
            {
                publishStatusURL += "publishStatus=1";

            }
            else if (viewChoice == "unpublished")
            {
                publishStatusURL += "publishStatus=2";
            }


            //Get search textbox text and insert into the URL on redirect. 
            string searchTitle = SearchByTitle.Text;
            string searchTitleFormat = searchTitle.Replace(" ", "+");
            
            Response.Redirect("~/Default.aspx?" + publishStatusURL + "&pageTitle="+searchTitleFormat);            
        }
    }
}
﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="MainNav.ascx.cs" Inherits="PagesDatabase.usercontrol.MainNav" %>


<asp:SqlDataSource
    runat="server"
    ID="pagesDB"
    ConnectionString="<%$ ConnectionStrings:pages_db %>"></asp:SqlDataSource>
<div class="navbar navbar-inverse navbar-fixed-top">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" runat="server" href="~/">Pages Database</a>
        </div>
        <div class="navbar-collapse collapse">
            <ul class="nav navbar-nav">
                <li><a runat="server" href="~/">View Pages</a></li>
                <li class="dropdown">
                    <%-- dropdown menu from BootStrap --%>
                    <a class="btn btn-secondary dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">View Page List
                    </a>
                    <ul runat="server" id="pagesdropdown" class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                        <asp:DataGrid runat="server" ID="PagesDataGrid" SkinID="nav"></asp:DataGrid>
                    </ul>
                </li>
                <li><a href="~/CreateNewPage" runat="server">Add Page</a></li>
            </ul>
        </div>
    </div>
</div>

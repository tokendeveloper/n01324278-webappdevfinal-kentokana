﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace PagesDatabase.usercontrol
{
    public partial class MainNav : System.Web.UI.UserControl
    {
        //Needless(?) to say, lots of code referenced from Christine's students/teachers/class DB example.

        //Set default query in a private string, and add property accessor.
        //pageId is selected by default, so that I can generate a link to the specific page url.
        //i.e. SinglePageView.aspx?pageId= <pageId>
        private string pages_basequery { get; set; } = "SELECT pageId, pageTitle AS 'Pages' FROM pages";

 

        protected void Page_Load(object sender, EventArgs e)
        {
            pagesDB.SelectCommand = pages_basequery;
            PagesDataGrid.DataSource = pageBind(pagesDB);
            PagesDataGrid.DataBind();
        }

        //pageBind function, which takes SqlDataSource as the parameter. 
        //Injected an entire table into the navigation dropdown menu.
        //It's probably not a very good idea as far as accessibility is concerned...
        protected System.Data.DataView pageBind(SqlDataSource src)
        {

            System.Data.DataTable table;
            System.Data.DataView view;
            table = ((System.Data.DataView)src.Select(DataSourceSelectArguments.Empty)).Table;

            //Generate dynamic link to specific pages.
            foreach (System.Data.DataRow row in table.Rows)
            {
                row["Pages"] =
                    "<li><a href=\"SinglePageView.aspx?pageId="
                    + row["pageId"]
                    + "\">"
                    + row["Pages"]
                    + "</a></li>";
            }
            //Remove pageId column.
            table.Columns.Remove("pageId");
            view = table.DefaultView;
            return view;
        }
    }
}
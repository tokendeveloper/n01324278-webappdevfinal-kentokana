﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="SinglePageView.aspx.cs" Inherits="PagesDatabase.WebForm1" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">


    <div runat="server" id="UpdateStatusDiv"></div>
    <div id="dataGridDiv2" class="jumbotron" runat="server">

        <%-- In order to make this usercontrol modular, I should I really put in the h2 tag in the .aspx file.  
        I'm just not sure how to get the h2 element from the aspx file to be recognized in the ascx file. --%>
        <h2 runat="server" id="pageHeading"></h2>

        <asp:SqlDataSource
            runat="server"
            ID="pagesDB"
            ConnectionString="<%$ ConnectionStrings:pages_db %>"></asp:SqlDataSource>
        <asp:DataGrid runat="server" ID="SinglePageDataGrid"></asp:DataGrid>
        <div id="contentDiv" runat="server"></div>
        <div runat="server" id="AuthorDiv">
            Written By: 
        </div>
        <div runat="server" id="DateDiv">
            Published On: 
        </div>
        <div runat="server" id="PageContentDiv">
        </div>

    </div>

    <asp:SqlDataSource
        runat="server"
        ID="deletePage"
        ConnectionString="<%$ ConnectionStrings:pages_db %>"></asp:SqlDataSource>

    <%-- Same thing here - I should really have the buttons inside aspx file so that this usercontrol is more modular. ah well.--%>

    <asp:Button class="buttonEl" runat="server" Text="Edit" ID="EditPageButton" OnClick="EditThisPage" />
    <%-- OnClientClick: referenced from Christine's example.
    Detects a click on the client-side, and triggers a confirm-box. If the box returns false, then it will return false
    (ie does not submit the form).--%>
    <asp:Button class="buttonEl" runat="server" Text="Delete Page"
        OnClientClick="if(!confirm('Are you sure you want to delete this entry?')) return false;"
        OnClick="DeletePage"
        value="Delete Page" />


    <div>
        <span><a runat="server" href="~/Default.aspx">Go Back</a></span>
    </div>
</asp:Content>

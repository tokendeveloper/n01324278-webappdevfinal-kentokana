﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="PageEdit.aspx.cs" Inherits="PagesDatabase.WebForm4" %>

<asp:Content ID="pageTitle" ContentPlaceHolderID="ContentTitle" runat="server">
    <h1>Edit Page</h1>
</asp:Content>

<asp:Content ID="EditContentForm" ContentPlaceHolderID="MainContent" runat="server">
    <div runat="server" id="UpdateStatus"></div>

    <asp:SqlDataSource
        runat="server"
        ID="UpdatePage"
        ConnectionString="<%$ ConnectionStrings:pages_db %>"></asp:SqlDataSource>
    <div class="jumbotron">
        <h2 id="debugDiv" runat="server">Select</h2>

        <h2 id="debugDiv2" runat="server">Update</h2>
    </div>

    <div class="form-group">
        <asp:Label runat="server" AssociatedControlID="PublishRadioBtn">Publish Page</asp:Label>
        <asp:RadioButton class="buttonEl" runat="server" GroupName="PublishState" ID="PublishRadioBtn" />
    </div>
    <div class="form-group">
        <asp:Label runat="server" AssociatedControlID="Unpublish">Unpublish</asp:Label>
        <asp:RadioButton class="buttonEl" runat="server" GroupName="PublishState" ID="Unpublish" />
    </div>


    <div class="form-group">
        <div>
            <asp:Label Text="Edit Page Title:" AssociatedControlID="titleForm" runat="server">Your Page Title:</asp:Label>
        </div>
        <asp:TextBox class="formEl" ID="titleForm" autopostback = "false"  runat="server" name="titleForm"></asp:TextBox>
    </div>
    <div class="form-group">
        <div>
            <asp:Label Text="Enter Author:" AssociatedControlID="authorForm" runat="server">Author:</asp:Label>
        </div>
        <asp:TextBox class="formEl" ID="authorForm" runat="server" />
        <asp:RequiredFieldValidator runat="server" CssClass="error-text" ID="RequiredFieldValidator1" ControlToValidate="authorForm" ErrorMessage="Please enter the author for this page."></asp:RequiredFieldValidator>
    </div>
    <div class="form-group">
        <div>
            <asp:Label Text="Edit Content:" AssociatedControlID="pageContentForm" runat="server">Your Page Content:</asp:Label>
        </div>
        <asp:TextBox class="formEl" ID="pageContentForm" TextMode="multiline" Columns="100" Rows="10" runat="server" />
    </div>
    <%--    https://stackoverflow.com/questions/4508051/textarea-control-asp-net-c-sharp --%>
    <div class="form-group">
        <asp:Button class="buttonEl" runat="server" Text="Finish Edit" OnClick="EditThisPage" ID="EditPageButton" />
    </div>

</asp:Content>
